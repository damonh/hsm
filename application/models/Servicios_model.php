<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * @package 	hsm
 *
 * @author 		Mauricio López Coria <mlopez@pronet.com.uy>
 * @copyright  	Copyright (c) 2018, Mauricio López Coria
 * @license    	GNU Public Licence v3
 * @version    	1.0
 *
 **/

class Servicios_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		$this->load->database();
	}

	public function get_servicios() {

		$query = $this->db->get('servicios');

		return $query->result_array();
	}

	public function get_servicios_recurrentes($isRecurrente = TRUE) {

		$query = $this->db->get_where('servicios',array('recurrente' => $isRecurrente));

		return $query->result_array();
	}

	/* Servicios recurrentes que tengan vencimientos pero no tengan pagos asociados */
	public function get_servicios_impagos() {

		
	}
}