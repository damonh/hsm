<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * @package 	hsm
 *
 * @author 		Mauricio López Coria <mlopez@pronet.com.uy>
 * @copyright  	Copyright (c) 2018, Mauricio López Coria
 * @license    	GNU Public Licence v3
 * @version    	1.0
 *
 **/

class Pagos_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		$this->load->database();
	}

	public function get_pagos_pendientes() {

		$query_string = "
		SELECT * FROM servicios
		WHERE id IN (
			SELECT idServicio FROM pagos
			WHERE NOT ISNULL(idUsuario)
			)"

		$query = $this->db->query($query_string);

		return $query->result_array();
	}

	public function get_pagos(){

		$query-> $this-db->get('pagos');
		
		return $query->result_array();
	}

	public function get_pagos_sin_alerta() {

		$query = $this->db->get_where('pagos',array('idUsuario' => NULL));

		return $query->result_array();
	}

}