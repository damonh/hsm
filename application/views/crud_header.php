<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.1.0/styles/github.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<?php
	if ( isset ($css_files) ){
		foreach ($css_files as $file): ?>
			<link rel="stylesheet" type="text/css" href="<?php echo $file; ?>" />
<?php 	endforeach; 
	}?>

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.1.0/highlight.min.js"></script>
	
</head>

<body>

<!-- Beginning of header -->
<div class="logo-img">
	 <img src="<?php echo $logo ?>" alt="Application Logo"> 
</div>

<div class="main-menu">
	<a href="<?php echo site_url() ?>">Inicio</a> | 
	<?php
	/* Extracts the last menu item to avoid showing a separator in the end */
	$last_item = array_pop($menu_items);
	foreach ($menu_items as $item): ?>
	<a href="<?php echo site_url('main/'.$item);?>"><?php echo (ucfirst($item));?></a> | 
	<?php 	endforeach; ?>
	<a href="<?php echo site_url('main/'.$last_item);?>"><?php echo (ucfirst($last_item));?></a>
</div>
<!-- End of header-->