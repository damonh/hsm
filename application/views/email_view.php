<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?=$title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
<p style="
		color: #565656;
		font-family: Georgia,serif;
		font-size: 16px;
		line-height: 25px;
		margin-top: 0;
		margin-bottom: 25px;">
	Estimado/a <?=$name;?>:
</p>
<p style="
		color: #565656;
		font-family: Georgia,serif;
		font-size: 16px;
		line-height: 25px;
		margin-top: 0;
		margin-bottom: 25px;
		margin-left: 5em">
	<?=$content;?>
</p>
<p style="
		color: #565656;
		font-family: Georgia,serif;
		font-size: 16px;
		line-height: 25px;
		margin-top: 0;
		margin-bottom: 25px;">
	Saludos cordiales,
</p>
<div style="
   		font-size: 26px;
   		font-weight: 700;
   		letter-spacing: -0.02em;
   		line-height: 32px;
   		color: #41637e;
   		font-family: sans-serif;
   		text-align: left">
   			<img style="
		   			border: 0;
		   			-ms-interpolation-mode: bicubic;
		   			display: block;
		   			margin-left: auto;
		   			Margin-right: auto;
		   			max-width: 152px"
				src="https://devel.sytes.net/assets/hostell-logo.png" alt="hostell-logo" width="152" height="108">
   		</div>
</div>

<table align="center" class="wrapper footer float-center" style="Margin:0;auto;border-collapse:collapse;border-spacing:0;float:none;margin:0;auto;padding:0;text-align:center;vertical-align:top;width:100%">
	<tr style="padding:0;text-align:left;vertical-align:top">
		<td class="wrapper-inner" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Lucida Grande,Geneva,Verdana,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
			<center data-parsed="" style="min-width:580px;width:100%">
				<table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%">
					<tbody>
					<tr style="padding:0;text-align:left;vertical-align:top">
						<td height="15px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Lucida Grande,Geneva,Verdana,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:15px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
					</tr>
					</tbody>
				</table>
				<p class="text-center float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#C8C8C8;font-family:Lucida Grande,Geneva,Verdana,sans-serif;font-size:12px;font-weight:400;line-height:16px;margin:0;margin-bottom:10px;padding:0;text-align:center">Sistema desarrollado por ProNet Tecnolog&iacute;a S.R.L.<br>Este es un correo enviado autom&aacute;ticamente, por favor no lo responda.</p>
			</center>
		</td>
	</tr>
</table>

</body>
</html>