<?php
 /**
 * 
 * @package 	hsm
 *
 * @author 		Mauricio López Coria <mlopez@pronet.com.uy>
 * @copyright  	Copyright (c) 2018, Mauricio López Coria
 * @license    	GNU Public Licence v3
 * @version    	1.0
 *
 **/

 class Scheduled extends CI_Controller {

 	public function __construct(){
		
		parent::__construct();
		if (!$this->input->is_cli_request()) show_error('Access denied');
      }

 	
 	public function message ($to = 'Mundo'){

 		echo "Hola {$to}!".PHP_EOL;
 	}

 	
 	public function check_client_alerts () {



 	}
 }