<?php

class Servicios extends CI_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->model('servicios_model');
		$this->load->helper('url_helper');
	}

	public function index() {

		$data['servicios'] = $this->servicios_model->get_servicios_recurrentes();
		$data['title'] = 'Servicios';

		$this->load->view('servicios/index',$data);
	}

/*
ToDo: Getting the n-days-to-expire services to generate vencimientos
*/

	function _send_reminder() {
		$this->load->library('email');

		$this->email->from('no-reply@pronet.com.uy', 'Systema de gestión de Servicios');
		$this->email->to('desarrollo@pronet.com.uy');

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		$this->email->send();
	}

}