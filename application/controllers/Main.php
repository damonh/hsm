<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Main extends CI_Controller {

	/* Properties */
	private $_menu = null;
	private $_css = null;
	private $_title = null;

	/* Functions */
	function __construct() {

		parent::__construct();

		/* Standard Libraries of codeigniter that are required */
		$this->load->database();
		$this->load->helper('url');
		/* Needed to date data type processing */
		$this->load->helper('date');
		
		$this->load->library('grocery_CRUD');
		
		/* Stores static properties used across the different CRUDs */
		$this->_css = base_url().'assets/grocery_crud/css/custom/views.css';
		$this->_title = 'Hostell';
		$this->_menu = array('pendientes', 'pagos', 'servicios','clientes',
			'contactos','planes','rubros','caracteristicas', 'usuarios');
		$this->_logo = base_url().'assets/hostell-logo.png';

		//$this->output->enable_profiler(TRUE);

	}

	public function index()	{
		$output = (object) array('title' => $this->_title, 'logo' => $this->_logo);

		$output->css_files[] = $this->_css;
		$output->menu_items = $this->_menu;

		$this->load->view('crud_header', $output);
		$this->load->view('crud_footer', $output);
	}

	public function clientes() {

		$crud = new grocery_CRUD();

		$crud->set_table('clientes');
		$crud->set_subject('Cliente','Clientes');

	/* Field order showed on Add/Edit actions */
		//$crud->fields('nombre','rubro','razonSocial','rut','telefono','direccion','detalles');
		$crud->required_fields('nombre');

	/* Column order showed on Lists */
		$crud->columns('nombre','rubro','razonSocial','rut','telefono','direccion','detalles');
		$crud->unset_columns('detalles', 'direccion', 'rut');
		$crud->display_as('idRubro', 'Rubro');
		$crud->display_as('razonSocial', 'Razón Social');
		$crud->display_as('telefono', 'Teléfono');
		$crud->display_as('rut', 'RUT');

	/* Table relations */
		$crud->set_relation('idRubro', 'rubros', 'nombre');
		/* For a 1_n relation you should use set_relation_n_n function like this:
	 	set_relation_n_n('field_name','selection_table','selection_table','current_pk','selection_table_pk','selection_table_field_name') */
		$crud->set_relation_n_n('contactos','contactos','contactos','id','idCliente','nombre');

		$output = $crud->render();

		$this->_output_template($output);
	}

	public function rubros() {

		$crud = new grocery_CRUD();

		$crud->set_subject('Rubro','Rubros');
		$crud->set_table('rubros');
		$crud->required_fields('Nombre');

		$output = $crud->render();

		$this->_output_template($output);
	}

	public function planes() {

		$crud = new grocery_CRUD();

		$crud->set_table('planes');
		$crud->set_subject('Plan','Planes');

		$crud->required_fields('nombre','costoSugerido','detalles');
		/* Table relations */
		$crud->set_relation_n_n('características','planes_caracteristicas',
			'caracteristicas','idPlan',
			'idCaracteristica','nombre');

		$crud->display_as('costoSugerido', 'Costo sugerido');

		$output = $crud->render();

		$this->_output_template($output);
	}

	public function caracteristicas() {

		$crud = new grocery_CRUD();

		$crud->set_subject('característica','características');
		$crud->set_table('caracteristicas');
		$crud->required_fields('nombre','valor');

		$output = $crud->render();

		$this->_output_template($output);
	}

	public function servicios()
	{
		$crud = new grocery_CRUD();

		$crud->set_subject('Servicio','Servicios');
		$crud->set_table('servicios');
		$crud->required_fields('nombre','fechaCompra','recurrente','idCliente','idPlan','costo');
		$crud->unset_columns('detalles');

		$crud->field_type('fechaVencimiento','hidden');

		$crud->display_as('idPlan', 'Plan');
		$crud->display_as('idCliente', 'Cliente');
		$crud->display_as('costo', 'Costo (USD)');
		$crud->display_as('fechaCompra', 'Compra');
		$crud->display_as('fechaVencimiento', 'Vencimiento');

		$crud->set_relation('idPlan', 'planes', 'nombre');
		$crud->set_relation('idCliente', 'clientes', 'nombre');

		$output = $crud->render();

		$this->_output_template($output);
	}

	public function contactos() {

		$crud = new grocery_CRUD();
		$crud->set_subject('contacto','contactos');
		$crud->set_table('contactos');
		$crud->required_fields('nombre','eMail1','celular1');
		$crud->display_as('idCliente','Cliente');
		$crud->display_as('eMail1','E-mail');
		$crud->display_as('eMail2','E-mail 2');
		$crud->display_as('celular1','Celular');
		$crud->display_as('celular2','Celular 2');
		$crud->display_as('direccion','Dirección');

		$crud->set_relation('idCliente','clientes','nombre');

		$output = $crud->render();

		$this->_output_template($output);
	}

	public function pagos() {

		$crud = new grocery_CRUD();

		$crud->set_subject('pago','pagos');
		$crud->set_table('pagos');
		//$crud->where(['fechaPago'] > mdate('%Y-01-01',now()));

		$crud->where('idUsuario IS NOT NULL',NULL,FALSE);


		/* Prevent user to add, delete or prevent all operations (listing only)
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_operations(); */
		
		$crud->unset_operations();
		
		$crud->display_as('idServicio','Servicio');
		$crud->display_as('idUsuario','Cobrado');
		$crud->display_as('ultimaAlerta','Última alerta');

		$crud->set_relation('idServicio','servicios','nombre');
		$crud->set_relation('idUsuario','usuarios','nombre');

		$output = $crud->render();

		$this->_output_template($output);
	}

	public function pendientes() {

		$crud = new grocery_CRUD();

		$crud->set_subject('pago pendiente','pagos pendientes');
		$crud->set_table('pagos');

		$crud->where('idUsuario' , NULL);

		$crud->unset_add();
		$crud->unset_delete();

		$crud->required_fields('idUsuario','fechaPago');

		// Disable fields on edit
		$crud->callback_edit_field('idServicio',
			function($value){return '<input disabled value="'.$value.'"></input>';});
		$crud->callback_edit_field('monto',
			function($value){return '<input disabled value="'.$value.'"></input>';});
		$crud->callback_edit_field('generacion',
			function($value){return '<input disabled value="'.$value.'"></input>';});
		$crud->callback_edit_field('ultimaAlerta',
			function($value){return '<input disabled value="'.$value.'"></input>';});

		$crud->set_rules('fechaPago','Fecha de pago','required|callback_check_expire_date['.$this->input->post('fechaPago').']');

		$crud->display_as('idServicio','Servicio');
		$crud->display_as('idUsuario','Cobrador');
		$crud->display_as('ultimaAlerta','Última alerta');
		$crud->display_as('fechaPago','Fecha de pago');

		$crud->set_relation('idServicio','servicios','nombre');
		$crud->set_relation('idUsuario','usuarios','nombre');

		$output = $crud->render();

		$this->_output_template($output);
	}

	function check_expire_date($expire_date)
	{
		echo('fechaPago: '.$this->input->post('fechaPago'));
		if ( ! empty($expire_date))
		{
			if ($expire_date > date("d/m/Y"))
			{
				$this->form_validation->set_message('check_expire_date', 'La fecha de pago no puede posterior a hoy');

			}
		}
	}

	public function usuarios() {

		$crud = new grocery_CRUD();

		$crud->set_subject('usuario','usuarios');
		$crud->set_table('usuarios');

		$crud->change_field_type('contraseña','password');

		$crud->callback_before_insert(array($this,'encrypt_password'));

		$output = $crud->render();

		$this->_output_template($output);
	}

	function encrypt_password ($post_array, $primary_key = null) {

		$this->load->helper('security');
		$post_array['contraseña'] = do_hash($post_array['contraseña'], 'md5');
		return $post_array;
	}

	function _output_template($output = null) {

		/* Not used in the views */
		$output->title = $this->_title;
		
		$output->css_files[] = $this->_css;
		$output->menu_items = $this->_menu;
		$output->logo = $this->_logo;

		$this->load->view('crud_header', $output);
		$this->load->view('crud_main', $output);
		$this->load->view('crud_footer', $output);
	}
}