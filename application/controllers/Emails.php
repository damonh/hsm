<?php

class Emails extends CI_Controller {

	private $_sender_address = 'no-reply@pronet.com.uy';

	private $_sender_name = 'Hostell';

	public function __construct() {
		
		parent::__construct();
		$this->load->library('email');
		$this->load->helper('url_helper');
	}

	public function index() {
		redirect('emails/send_email');
	}

	function send_email($subject = 'Alerta de vencimiento', $recipient = 'pfeola@pronet.com.uy', $content = 'Este es un mensaje de prueba.') {
		
		$this->email->from($this->_sender_address, $this->_sender_name);
		$this->email->to($recipient);
		$this->email->subject($subject);
		$this->email->set_header('X-ProNet-Header', 'TRUE');
		
		$content = "Este es un mensaje automatizado para recordarle que en 15 días vence su servicio de hosting [xxx]. <br />Por favor, contáctese con ProNet Tecnología para confirmar su renovación y evitar interrupciones en sus servicios";

		$data['title'] = 'Hostell - Gestión de Servicios de Hosting';
		$data['name'] = 'Paula Feola';
		$data['content'] = $content;
		$msg = $this->load->view('email_view',$data,TRUE);

		$this->email->message($msg);

		// To let print_debugger show the message, send must be called with a FALSE argument
		$this->email->send(FALSE);

		echo $this->email->print_debugger();
	}

}